﻿using System;
using System.Windows.Data;

namespace Task.Converters
{
    [ValueConversion(typeof(double), typeof(string))]
    public class FormConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value?.ToString() ?? string.Empty;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (string.IsNullOrEmpty((string) value))
            {
                return 0;
            }

            if (double.TryParse((string)value, out double returnedValue))
            {
                return returnedValue;
            }

            throw new Exception("The text is not a number");
        }
    }
}
