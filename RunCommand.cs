﻿using System;
using System.Windows.Input;

namespace Task
{
    public class RunCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;
        private readonly Action _action;

        public RunCommand(Action action)
        {
            _action = action;
        }
        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            _action.Invoke();
        }
    }
}
