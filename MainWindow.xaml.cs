﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Task
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void NumberValidationForTextBox(object sender, DataObjectPastingEventArgs e)
        {
            if (!IsNumber((string) e.DataObject.GetData(typeof(string))))
            {
                e.CancelCommand();
            }
        }

        private void NumberValidationForTextBox(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsNumber(((TextBox) sender).Text + e.Text);
        }

        public bool IsNumber(string text)
        {
            return double.TryParse(text, out double d);
        }

    }
}
