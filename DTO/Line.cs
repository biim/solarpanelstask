﻿using System;
using System.Collections.Generic;
using NetTopologySuite.Geometries;

namespace Task.DTO
{
    public class Line
    {
        public Coordinate Start { get; set; }
        public Coordinate End { get; set; }

        public static List<Line> MakeRectangle(Coordinate startingPoint, double length, double width)
        {
            var a = new Line
            {
                Start = new Coordinate(startingPoint.X, startingPoint.Y),
                End = new Coordinate(startingPoint.X, startingPoint.Y - length)
            };
            var b = new Line
            {
                Start = a.End,
                End = new Coordinate(a.End.X + width, a.End.Y)
            };
            var c = new Line
            {
                Start = b.End,
                End = new Coordinate(b.End.X, b.End.Y + length)
            };
            var d = new Line
            {
                Start = c.End,
                End = a.Start
            };

            return new List<Line>
            {
                a,b,c,d
            };
        }

        public List<Coordinate> GetInBetweenCoordinates(int quantity)
        {
            var coordinates = new List<Coordinate>();

            var diffY = End.Y - Start.Y;
            var diffX = End.X - Start.X;
            var slope = (End.Y - Start.Y) / (End.X - Start.X);
            quantity = quantity - 1;

            for (double i = 0; i < quantity; i++)
            {
                var y = slope == 0 ? 0 : diffY * (i / quantity);
                var x = slope == 0 ? diffX * (i / quantity) : y / slope;

                var xCoordinate = Math.Round(x) + Start.X;
                var yCoordinate = Math.Round(y) + Start.Y;
                coordinates.Add(new Coordinate(xCoordinate, yCoordinate));
            }

            coordinates.Add(End);
            return coordinates;
        }

        public bool Intersects(Line b)
        {
            return IsIntersecting((float) Start.X, (float)Start.Y, (float)End.X, (float)End.Y, (float)b.Start.X, (float)b.Start.Y, (float)b.End.X, (float)b.End.Y);
        }

        public static bool IsIntersecting(float ax, float ay, float bx, float by, float cx, float cy, float dx, float dy)
        {
            float denominator = (bx - ax) * (dy - cy) - (by - ay) * (dx - cx);
            float numerator1 = (ay - cy) * (dx- cx) - (ax - cx) * (dy - cy);
            float numerator2 = (ay - cy) * (bx - ax) - (ax - cx) * (by - ay);

            if (denominator == 0)
            {
                return numerator1 == 0 && numerator2 == 0;
            }

            float r = numerator1 / denominator;
            float s = numerator2 / denominator;

            return r >= 0 && r <= 1 && s >= 0 && s <= 1;
        }
        
        public static (bool, bool) InBoundary(Polygon boundary, List<Line> poly)
        {
            var polyLines = ParseLineString(poly);
            var intercepts = polyLines.Intersects(boundary);
            var isIn = polyLines.Within(boundary);
            return (intercepts, isIn);
        }

        private static Polygon ParseLineString(List<Line> poly)
        {
            var boundary = new List<Coordinate>();
            poly.ForEach(x => {
                boundary.Add(x.Start);
                boundary.Add(x.End);
            });
            return new Polygon(new LinearRing(boundary.ToArray()));
        }
    }
}
