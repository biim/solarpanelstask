﻿namespace Task.DTO
{
    public class CoordinateDto
    {
        public int X { get; set; }
        public int Y { get; set; }
    }
}
