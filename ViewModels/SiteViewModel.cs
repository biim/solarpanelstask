﻿using System.Collections.Generic;
using Task.DTO;

namespace Task.ViewModels
{
    public class SiteViewModel : SiteViewModelBase
    {
        public override bool InSiteBounds(List<Line> lines)
        {
            var (_, isIn) = Line.InBoundary(Site, lines);
            return isIn;
        }

        public override bool InRestrictedBounds(List<Line> lines)
        {
            var (intercepts, isIn) = Line.InBoundary(Restriction, lines);
            return intercepts || isIn;
        }
    }
}
