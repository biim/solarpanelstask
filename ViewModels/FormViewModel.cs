﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Input;
using Task.Annotations;

namespace Task.ViewModels
{
    public class FormViewModel : INotifyPropertyChanged
    {
        private double? _width;
        private double? _length;
        private double? _tiltAngle;
        private double? _rowSpacing;
        private double? _columnSpacing;
        private string _message;

        public SiteViewModelBase Site { get; }
        
        private double TiltedWidth
        {
            get
            {
                if (_tiltAngle.HasValue)
                {
                    double angle = Math.PI * _tiltAngle.Value / 180.0;
                    return Math.Cos(angle) * _width.Value;
                }

                return _width.Value;
            }
        }

        public double? Width
        {
            get => _width;
            set => _width = value;
        }

        public double? Length
        {
            get => _length;
            set => _length = value;
        }

        public double? TiltAngle
        {
            get => _tiltAngle;
            set => _tiltAngle = value;
        }

        public double? RowSpacing
        {
            get => _rowSpacing;
            set => _rowSpacing = value;
        }

        public double? ColumnSpacing
        {
            get => _columnSpacing;
            set => _columnSpacing = value;
        }

        public ICommand ButtonCommand { get; set; }

        public string Message
        {
            get => _message;
            set
            {
                _message = value;
                OnPropertyChanged(nameof(Message));
            }
        }

        public FormViewModel()
        {
            //If true will not use NetTopologySuite library to calculate InBounds
            bool useManual = true;

            ButtonCommand = new RunCommand(Generate);
            if (useManual)
            {
                Site = new ManualSiteViewModel();
            }
            else
            {
                Site = new SiteViewModel();
            }
        }

        public void Generate()
        {
            if (!IsValid())
            {
                return;
            }

            Site.Fill(TiltedWidth, _length ?? 0, _rowSpacing ?? 0, _columnSpacing ?? 0);

            Message = Site.ResultLines.Count == 0 ? "No panels could fit" : $"Task complete, generated {Site.ResultLines.Count / 4} panels";
        }

        private bool IsValid()
        {
            var errorMsg = MissingFields;
            if (string.IsNullOrEmpty(errorMsg))
            {
                return true;
            }

            errorMsg = errorMsg.IndexOf(",", StringComparison.Ordinal) > 0 ? 
                $"{errorMsg} fields have illogical values" : $"{errorMsg} field has illogical value";

            MessageBox.Show(errorMsg, "Please make sure all required fields are filled",
                MessageBoxButton.OK, MessageBoxImage.Warning);
            return false;
        }

        private string MissingFields
        {
            get
            {
                var fields = string.Empty;
                if (!_width.HasValue || _width.Value == 0)
                {
                    fields = AppendField(fields,"Width");
                }

                if (!_length.HasValue || _length.Value == 0)
                {
                    fields = AppendField(fields, "Length");
                }

                if (!_rowSpacing.HasValue || _rowSpacing.Value == 0)
                {
                    fields = AppendField(fields, "Row Spacing");
                }

                if (!_columnSpacing.HasValue || _columnSpacing.Value == 0)
                {
                    fields = AppendField(fields, "Column Spacing");
                }

                if (_tiltAngle >= 90 || _tiltAngle < 0)
                {
                    fields = AppendField(fields, "Tilt Angle");
                }

                return fields;
            }
        }

        private string AppendField(string currentValue, string field)
        {
            if (currentValue.Length > 0)
            {
                return $"{currentValue}, {field}";
            }

            return $"{field}";
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
