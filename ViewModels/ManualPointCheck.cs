﻿using System.Collections.ObjectModel;
using NetTopologySuite.Geometries;
using System.Collections.Generic;
using System.Linq;
using Task.DTO;

namespace Task.ViewModels
{
    public class ManualSiteViewModel : SiteViewModelBase
    {
        public override bool InSiteBounds(List<Line> lines)
        {
            foreach (var line in lines)
            {
                var points = line.GetInBetweenCoordinates(10 * Scale);
                if (!points.All(y => InBounds(SiteLines, y)))
                {
                    return false;
                }
            }

            return true;
        }

        public override bool InRestrictedBounds(List<Line> lines)
        {
            foreach (var line in lines)
            {
                var points = line.GetInBetweenCoordinates(10 * Scale);
                if (points.Any(x => InBounds(RestrictionLines, x)))
                {
                    return true;
                }
            }

            return false;
        }

        private bool InBounds(ObservableCollection<Line> boundary, Coordinate point)
        {
            var infPoint = new Coordinate(int.MaxValue, point.Y);
            var newLine = new Line
            {
                Start = point,
                End = infPoint
            };

            var intersecting = boundary.Count(x => x.Intersects(newLine));
            return intersecting % 2 > 0;
        }
    }
}
