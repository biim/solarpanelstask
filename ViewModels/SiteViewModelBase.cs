﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using NetTopologySuite.Geometries;
using Newtonsoft.Json;
using Task.DTO;

namespace Task.ViewModels
{
    public abstract class SiteViewModelBase
    {
        public readonly int Scale = 2;

        public Polygon Site { get; }
        public Polygon Restriction { get; }

        public ObservableCollection<Line> SiteLines { get; }
        public ObservableCollection<Line> RestrictionLines { get; }
        public ObservableCollection<Line> ResultLines { get; set; }

        protected SiteViewModelBase()
        {
            ResultLines = new ObservableCollection<Line>();
            var restrictionCoordinates = ReadCoordinatesFromFile("RestrictionCoordinates.json");
            var siteCoordinates = ReadCoordinatesFromFile("SiteCoordinates.json");

            Site = new Polygon(new LinearRing(siteCoordinates));
            Restriction = new Polygon(new LinearRing(restrictionCoordinates));

            SiteLines = CalculateSiteLines(siteCoordinates);
            RestrictionLines = CalculateSiteLines(restrictionCoordinates);
        }

        private ObservableCollection<Line> CalculateSiteLines(Coordinate[] coordinates)
        {
            var result = new ObservableCollection<Line>();
            for (int i = 1; i < coordinates.Length; i++)
            {
                result.Add(new Line
                {
                    Start = new Coordinate
                    {
                        Y = coordinates[i - 1].Y,
                        X = coordinates[i - 1].X,
                    },
                    End = new Coordinate
                    {
                        Y = coordinates[i].Y,
                        X = coordinates[i].X,
                    }
                });
            }

            return result;
        }

        private Coordinate[] ReadCoordinatesFromFile(string fileName)
        {
            var path = Path.Combine(Directory.GetCurrentDirectory(), fileName);
            if (File.Exists(path))
            {
                try
                {
                    var fileText = File.ReadAllText(path);
                    var parsedCoordinates = JsonConvert.DeserializeObject<List<CoordinateDto>>(fileText);
                    var result = parsedCoordinates?.Select(x => new Coordinate(x.X * Scale, x.Y * Scale)).ToList();
                    return result?.ToArray();
                }
                catch
                {
                    return null;
                }
            }

            return null;
        }

        public ObservableCollection<Line> Fill(double width, double length, double rowSpacing, double columnSpacing)
        {
            ResultLines.Clear();

            var result = new ObservableCollection<Line>();
            width *= Scale;
            length *= Scale;
            rowSpacing *= Scale;
            columnSpacing *= Scale;

            var minX = SiteLines.Min(x => x.Start.X);
            var maxY = SiteLines.Max(x => x.Start.Y);
            var startingPoint = new Coordinate(minX, maxY);

            while (true)
            {
                var row = FillRow(startingPoint, width, length, rowSpacing);
                if (row.Any())
                {
                    row.ForEach(ResultLines.Add);
                    startingPoint.Y = row.First().End.Y;
                    startingPoint.Y -= columnSpacing;
                }
                else
                {
                    startingPoint.Y--;
                }

                if (TooFar(startingPoint))
                {
                    break;
                }
            }

            return result;
        }

        private List<Line> FillRow(Coordinate startingPoint, double width, double length, double rowSpacing)
        {
            var result = new List<Line>();
            while (true)
            {
                var rect = RowRectangle(startingPoint, width, length);
                if (!rect.Any())
                {
                    return result;
                }

                result.AddRange(rect);
                var last = rect.Last().Start;
                startingPoint = new Coordinate(last.X, last.Y);
                startingPoint.X += rowSpacing;

                if (TooFar(startingPoint))
                {
                    break;
                }
            }

            return result;
        }

        private List<Line> RowRectangle(Coordinate startingPoint, double width, double length)
        {
            var X = startingPoint.X;
            var Y = startingPoint.Y;

            while (true)
            {
                var rect = Line.MakeRectangle(new Coordinate(X, Y), width, length);
                if (
                    InSiteBounds(rect) &&
                    !InRestrictedBounds(rect))
                {
                    return rect;
                }

                X = rect[0].Start.X + 1;
                Y = rect[0].Start.Y;

                if (TooFar(rect.Last().Start))
                {
                    return new List<Line>();
                }
            }
        }

        private bool TooFar(Coordinate point)
        {
            var infPoint = new Coordinate(int.MaxValue, point.Y);
            var newLine = new Line
            {
                Start = point,
                End = infPoint
            };

            return !SiteLines.Any(x => x.Intersects(newLine));
        }

        public abstract bool InSiteBounds(List<Line> lines);
        public abstract bool InRestrictedBounds(List<Line> lines);
    }
}
